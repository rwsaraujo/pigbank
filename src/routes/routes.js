import React from 'react'
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom'
import HomePage from '../pages/HomePage/HomePage'
import Quotation from '../pages/Quotation/Quotation'
import ComingSoon from '../pages/ComingSoon/ComingSoon'
import NotFound from '../pages/404/404'

function Routes() {
    return (
        <Router>
            <Switch>
                <Route exact path='/'>
                    <HomePage/>
                </Route>

                <Route exact path='/quotation'>
                    <Quotation/>
                </Route>

                <Route exact path='/coming-soon'>
                    <ComingSoon/>
                </Route>

                <Route exact path='/404'>
                    <NotFound/>
                </Route>

                <Route path='/'>
                    <NotFound/>
                </Route>
            </Switch>
        </Router>
    )
}

export default Routes