import React from 'react';
import Header from '../../components/Header/Header';
import './HomePage.css'
import PigBank from '../../assets/images/group-1.svg';
import PeopleMoney from '../../assets/images/group-2.svg'
import PinkButton from '../../components/Buttons/PinkButton/PinkButton'
import Footer from '../../components/Footer/Footer'


function HomePage() {
    return (
        <div>
            <header>
                <Header/>
            </header>
            <div className="sectionOne">
                <div>
                    <img src={PigBank} alt="pigBank"/>
                </div>
                <div className="textButton">
                    <h2>A mais nova alternativa de banco digital chegou!</h2>
                    <p>Feito para caber no seu bolso e otimizar seu tempo. O PigBank veio pra ficar!</p>
                    <a href="/em-breve"><PinkButton>Abra sua conta</PinkButton></a>
                </div>
            </div>

            <div className="sectionTwo">
                <div className="sectionTwoTitle">
                    <hr />
                    <h2>Fique por dentro!</h2>
                    <hr />
                </div>
                <p>Ao contrário do ditado popular, por aqui, quem se mistura com porco não come farelo! Conheça nossa plataforma exclusivamente dedicada para ampliar o seu patrimônio.</p>

                <img src={PeopleMoney} alt=""/>

                <a href="/cotacao-moedas"><PinkButton>Cotação Moedas</PinkButton></a>

                <footer>
                    <Footer/>
                </footer>
            </div>
        </div>
    )
}

export default HomePage
