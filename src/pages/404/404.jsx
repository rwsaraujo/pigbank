import React from 'react'
import Header from '../../components/Header/Header'
import Footer from '../../components/Footer/Footer'
import ConfusedPig from '../../assets/images/confused-pig.svg'
import PinkButton from '../../components/Buttons/PinkButton/PinkButton'

function NotFound() {
    return (
        <>
            <header>
                <Header/>
            </header>

            <div className="main">
                <h2>Erro 404</h2>
                <img src={ConfusedPig} alt="" />
                <p>Ops! Parece que o endereço que você digitou está incorreto...</p>
                <a href="/"><PinkButton>Voltar</PinkButton></a>
            </div>

            <footer>
                <Footer/>
            </footer>
        </>
    )
}

export default NotFound
