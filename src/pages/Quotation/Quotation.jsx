import React from 'react'
import Header from '../../components/Header/Header'
import Footer from '../../components/Footer/Footer'
import Cards from './Cards/Cards'

function Quotation() {
    return (
        <div>
            <header>
                <Header/>
            </header>

            <Cards/>

            <footer>
                <Footer/>
            </footer>
        </div>
    )
}

export default Quotation
