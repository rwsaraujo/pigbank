import React from 'react'
import Header from '../../components/Header/Header'
import Footer from '../../components/Footer/Footer'
import PigConstruction from '../../assets/images/building-construction.svg'
import PinkButton from '../../components/Buttons/PinkButton/PinkButton'
import './ComingSoon.css'

function ComingSoon() {
    return (
        <>
            <header>
                <Header/>
            </header>

            <div className="main">
                <h2>Em Construção</h2>
                <img src={PigConstruction} alt="" />
                <p>Parece que essa página ainda não foi implementada... Tente novamente mais tarde!</p>
                <a href="/"><PinkButton>Voltar</PinkButton></a>
            </div>

            <footer>
                <Footer/>
            </footer>
        </>
    )
}

export default ComingSoon
