import React from 'react'
import './Header.css'
import PinkButton from '../Buttons/PinkButton/PinkButton'
import WhiteButton from '../Buttons/WhiteButton/WhiteButton'

function Header() {
    return (
        <>
            <header>
                <nav>
                    <div className="logo">
                        <h1><a href="/">Pigbank</a></h1>
                    </div>
                    <div>
                        <a href="/quotation"><PinkButton>Cotação Moedas</PinkButton></a>
                        <a href="/coming-soon"><WhiteButton>Acessar</WhiteButton></a>
                    </div>
                </nav>
            </header>
        </>
    )
}

export default Header
