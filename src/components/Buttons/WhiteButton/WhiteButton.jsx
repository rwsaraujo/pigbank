import React from 'react'
import './WhiteButton.css'

function WhiteButton(props) {
    return (
        <>
            <button className="btn2">
                {props.children}
            </button>
        </>
    )
}

export default WhiteButton
