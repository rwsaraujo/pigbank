import React from 'react'
import './PinkButton.css'

function PinkButton(props) {
    return (
        <>
            <button className="btn1">
                {props.children}
            </button>
        </>
    )
}

export default PinkButton
